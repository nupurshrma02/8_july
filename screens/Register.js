import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, StyleSheet, Button, Alert, TouchableOpacity } from 'react-native';
import {Appbar, TextInput} from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';

const Profile = ({navigation}) => {
    const [userName, setuserName] = React.useState('');
    const [Email, setEmail] = React.useState('');
    const [Password, setPassword] = React.useState('');
    const [Age, setAge] = React.useState('');
    const [Address, setAddress] = React.useState('');


    const myfun = async() => {
      //Alert.alert(petname);
      await fetch('http://3.12.158.241/medical/api/register',{
          method : 'POST',
          headers:{
              'Accept': 'application/json',
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            "name":userName,
            "email":Email,
            "password":Password,
            //"age"     :Age,
            //"address" :Address,
                        
       })
      }).then(res => res.json())
      .then(resData => {
         console.log(resData);
         Alert.alert(resData.message);
         if(resData.message === 'User Created Successfully'){
          navigation.navigate('LoginScreen')
         }
      });
   }
    return (
      <View style={{backgroundColor:'#307ecc', flex:1}}>
          <View style={{alignItems: 'center'}}>
          {/* <Image
                source={require('../Image/3.jpg')}
                style={{
                  width: '50%',
                  height: 100,
                  resizeMode: 'contain',
                  margin: 30,
                }}
              /> */}
              </View>
        <ScrollView style={styles.contentBody}>
            <TextInput
                label="Name"
                value={userName}
                onChangeText={userName => setuserName(userName)}
                />
                <Text/>
            <TextInput
                label="Email"
                value={Email}
                onChangeText={Email => setEmail(Email)}
                />
                <Text/>
            <TextInput
                label="Password"
                value={Password}
                onChangeText={Password => setPassword(Password)}
                />
                <Text/>
            {/* <TextInput
                label="Age"
                value={Age}
                onChangeText={Age => setAge(Age)}
                />
                <Text/> */}
            {/* <TextInput
                label="Address"
                value={Address}
                onChangeText={Address => setAddress(Address)}
                />
                <Text/> */}
            <Text />
            <Button
                onPress={myfun}
                title="Signup"
                color="#7DE24E"
                accessibilityLabel="Learn more about this purple button"
                />
            <View style={{alignItems: 'center'}}>
            <Text style={{color:'#fff'}}>Already a user?<TouchableOpacity onPress={()=>{navigation.navigate('Login')}}><Text style={styles.register}>Login</Text></TouchableOpacity></Text>
            </View>
        </ScrollView>
      </View>
    );
  }

export default Profile


const styles = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
    },
    contentBody: {
        padding: 15,
        //backgroundColor: '#307ecc'
    },
    threebox: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between'
    },
    boxwrap: {
        width: '100%'
    },
    singleBox: {
        width: '30%',
        backgroundColor: '#fff',
        marginBottom: 10,
        paddingHorizontal: 10,
        paddingVertical: 15,
        alignItems: 'center',
    },
    boxtext: {
        fontSize: 12,
        alignItems: 'center',
        width: '100%',
        textAlign: 'center'
    },

    register: {
      fontSize: 20,
      marginTop: 10,
      lineHeight: 30,
      position: 'relative',
      top: 7,
      paddingLeft : 10,
      color: '#fff'
    }
  });