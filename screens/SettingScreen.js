//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity,Button } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Modal from 'react-native-modal';




// create a component
const SettingScreen = () => {
    const [isModalVisible, setModalVisible] = React.useState(false);

    const toggleModal = () => {
        setModalVisible(!isModalVisible);
       
      };

    const modal = () => {
        return(
            <View style={{flex: 1}}>
          <Button title="Show modal" onPress={toggleModal} />
    
          <Modal isVisible={isModalVisible}>
            <View style={{flex: 1}}>
              <Text>Hello!</Text>
    
              <Button title="Hide modal" onPress={toggleModal} />
            </View>
          </Modal>
        </View>
            );
    };
    
    return (
            <View style={styles.container}>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('Display')}><Text style={styles.text}>Display/Default</Text></TouchableOpacity>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('Profile')}><Text style={styles.text}>Pilot Details/Ecrew-Login/Eupload</Text></TouchableOpacity>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('EGCAUpload')}><Text style={styles.text}>EGCA Upload</Text></TouchableOpacity>
                <TouchableOpacity><Text style={styles.text} onPress={()=> this.props.navigation.navigate('BuildLogbook')} >Build LogBook</Text></TouchableOpacity>
                <TouchableOpacity><Text style={styles.text}>Gallery</Text></TouchableOpacity>
                <TouchableOpacity><Text style={styles.text}>Support/Backup</Text></TouchableOpacity>
                <View style={{flexDirection:'row'}}>
                <Text onPress={()=> this.props.navigation.navigate('EGCAUpload')} style={styles.text}>Help Videos</Text>
                <TouchableOpacity onPress={toggleModal, modal}>
                <MaterialCommunityIcons name="head-question" color={'red'} size={30} style={{marginLeft: 200}} />
                </TouchableOpacity>
                </View>           
            </View>
        );
    }


// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        //backgroundColor: '#2c3e50',
    },
    text: {
        fontSize: 25,
    },

});

//make this component available to the app
export default SettingScreen;
